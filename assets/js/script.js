(function($){
    $( window ).resize(function() {
        headerScrolled();
        

    });

    $( document ).ready(function() {
        headerScrolled();
        
        $('#contact-form').on('submit',function(event){
            // cancels the form submission
            event.preventDefault();
            var form = $(this);
            console.log(form.serialize());
            
            $.ajax({
                url: "./assets/email.php",
                type: "POST",
                data: form.serialize(),
                success: function(result){
                    console.log(result);
                    if (result == 'success'){
                        console.log("listo");
                        jQuery("#successForm").removeClass("d-none")
                        form.addClass("d-none");
                    } else {
                        console.log("error");
                    }
                }
            });
            

            return false;   
        });
    });
    
    $(document).scroll(function(){
        
        headerScrolled();
    });

    function headerScrolled(){
        if( $(window).scrollTop() < 200 ){
            $("#header").removeClass("scrolled");
        }else{
            $("#header").addClass("scrolled");
        }
    }
})(jQuery); 